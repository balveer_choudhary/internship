**Project Setup**

Download repository

Using:   `git clone https://gitlab.com/balveer_choudhary/internship.git`

There are three sub folder in downloaded repository 

1. quillpadserver 
2. backend
3. frontend


///////////////////////////// 1. setup for quillpad server /////////////////////////////////

quillpadserver directory have local Quillpad Transliteration Server run on localhost port 8090
Setup for run Quillpad Transliteration Server 

Download required pyrex packaeg from this link [click here](http://www.cosc.canterbury.ac.nz/greg.ewing/python/Pyrex/Pyrex-0.9.9.tar.gz)

Then follow these commands 

```
 cd Downloads && tar -xvf Pyrex-0.9.9.tar.gz
 cd Pyrex-0.9.9
 sudo python setup.py install
 cd internship && cd quillpadserver
 pip install cherrypy
 sudo apt-get install python-mysqldb
 python startquill_cherry.py
```


Additional Information
Quillpad runs on port number 8090 (Additional configuration parameters are in quill_cherry8088.conf)
processWordJSON and processWord are the API endpoints over which the transliteration server can be accessed.
Example:
 * localhost:8090/processWordJSON?inString=hello&lang=hindi
 * localhost:8090/processWordJSON?inString=hello&lang=kannada



/////////////////////////////// 2. Setup for frontend (Vue.js) ///////////////////////////////////// 

Follow these commands 

```
 Cd internship && cd frontend
 npm install
 npm run serve
```


Frontend run on localhost:8080/




////////////////////////////////////// 3. Setup for backend (Adonis.js) /////////////////////////////////////////// 

Follow these commands 
```
 cd internship && cd backend
 npm install
 adonis serve --dev
```



Backend run on localhost:3333


Configuration in backend 

Create a .env file in backend dir. 

///// .env file content/////

```
HOST=127.0.0.1
PORT=3333
NODE_ENV=development
APP_NAME=AdonisJs
APP_URL=http://${HOST}:${PORT}
CACHE_VIEWS=false
APP_KEY=dYHIKMs0RkOuBVkxKSdP6vCelGoqr7Ca
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_USER=”DB USER NAME”
DB_PASSWORD=”DB USER PASSWORD”
DB_DATABASE=transliteration
HASH_DRIVER=bcrypt
```




Lamp or  xampp  is required 

Make a database in phpmyadmin

Database name =  “transliteration” 


Run this command to make tables in database
 `cd internship/backend && adonis migration:run`

*Allow CORS: Access-Control-Allow-Origin* browser extension needed due to http request


If there is any confusion or query regarding this project setup you can mail me.
Mail Id :  balveerchoudhary9672@gmail.com
